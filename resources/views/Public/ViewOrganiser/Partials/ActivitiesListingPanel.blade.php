<section class="">
    <div class="event-title">
        <h1>國是論壇場次</h1>
    </div>
    <div class="cards">
         @if(count($events))
            @foreach($events->where('is_live', 1) as $event)
                <button class="card" onclick="javascript:location.href='{{$event->event_url }}'">
                    <a href="{{$event->event_url }}" class="title">{{ $event->title }}</a>
                        <div class="info">
                            <div>{{ $event->start_date->format('Y') }} / {{ explode("|", trans("basic.months_short"))[$event->start_date->format('n')] }} /  {{ $event->start_date->format('d') }}</div>
                            <div>地點</div>
                        </div>
                </button>
            @endforeach
         @endif
    </div>
</section>
