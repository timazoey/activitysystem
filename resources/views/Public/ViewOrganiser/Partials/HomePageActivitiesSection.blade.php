<section  class="container" style="background-color:transparent">
    <div class="row" style=display:flex;>
        <div class="col-xs-12 col-md-8" style=margin:auto;width:100%>
            @include('Public.ViewOrganiser.Partials.ActivitiesListingPanel',
                [
                    'events'      => $upcoming_events
                ]
            )
        </div>
    </div>
</section>