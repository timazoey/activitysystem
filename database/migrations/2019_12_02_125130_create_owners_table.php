<?php

use Illuminate\Database\Migrations\Migration;

class CreateOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        /*
         * Owners Table
         */
        Schema::create('owners', function ($t) {

            $t->increments('id');
            $t->unsignedInteger('account_id')->index();
            $t->nullableTimestamps();
            $t->softDeletes();

            $t->string('first_name')->nullable();
            $t->string('last_name')->nullable();
            $t->string('phone')->nullable();
            $t->string('email');
            $t->string('password');
            $t->string('confirmation_code');
            $t->boolean('is_registered')->default(false);
            $t->boolean('is_confirmed')->default(false);
            $t->boolean('is_parent')->default(false);
            $t->string('remember_token', 100)->nullable();

            $t->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = [
            'order_statuses',
            'ticket_statuses',
            'reserved_tickets',
            'timezones',
            'date_formats',
            'datetime_formats',
            'currencies',
            'accounts',
            'owners',
            'organisers',
            'events',
            'orders',
            'tickets',
            'order_items',
            'ticket_order',
            'event_stats',
            'attendees', 
            'messages',
            'event_images',
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        foreach ($tables as $table) {
            Schema::drop($table);
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
