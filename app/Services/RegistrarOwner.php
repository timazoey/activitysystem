<?php

namespace App\Services;

use App\Models\Owner;
use Illuminate\Contracts\Auth\RegistrarOwner as RegistrarContract;
use Validator;

class RegistrarOwner implements RegistrarContract
{
    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => 'required|max:255',
            'email'    => 'required|email|max:255|unique:owners',
            'password' => 'required|confirmed|min:8',
        ]);
    }

    /**
     * Create a new owner instance after a valid registration.
     *
     * @param array $data
     *
     * @return owner
     */
    public function create(array $data)
    {
        return Owner::create([
            'name'     => $data['name'],
            'email'    => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
