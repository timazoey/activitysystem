<?php

namespace App\Http\Controllers;

use App\Attendize\Utils;
use App\Models\Account;
use App\Models\Owner;
use Hash;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Mail;

class OwnerSignupController extends Controller
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        if (Account::count() > 0 && !Utils::isAttendize()) {
            return redirect()->route('login');
        }

        $this->auth = $auth;
        $this->middleware('guest');
    }

    public function showSignup()
    {
        $is_attendize = Utils::isAttendize();
        return view('Public.OwnerLoginAndRegister.Signup', compact('is_attendize'));
    }

    /**
     * Creates an account.
     *
     * @param Request $request
     *
     * @return Redirect
     */
    public function postSignup(Request $request)
    {
        $is_attendize = Utils::isAttendize();
        $this->validate($request, [
            'email'        => 'required|email|unique:owners',
            'password'     => 'required|min:8|confirmed',
            'first_name'   => 'required',
            'terms_agreed' => $is_attendize ? 'required' : '',
        ]);

        $account_data = $request->only(['email', 'first_name', 'last_name']);
        $account_data['currency_id'] = config('attendize.default_currency');
        $account_data['timezone_id'] = config('attendize.default_timezone');
        $account = Account::create($account_data);

        $owner = new Owner();
        $owner_data = $request->only(['email', 'first_name', 'last_name']);
        $owner_data['password'] = Hash::make($request->get('password'));
        $owner_data['account_id'] = $account->id;
        $owner_data['is_parent'] = 1;
        $owner_data['is_registered'] = 1;
        $owner = Owner::create($owner_data);

        if ($is_attendize) {
            // TODO: Do this async?
            Mail::send(
                'Emails.ConfirmEmail',
                ['first_name' => $owner->first_name, 'confirmation_code' => $owner->confirmation_code],
                function ($message) use ($request) {
                    $message->to($request->get('email'), $request->get('first_name'))
                        ->subject(trans("Email.attendize_register"));
                }
            );
        }

        session()->flash('message', 'Success! You can now login.');

        return redirect('login');
    }

    /**
     * Confirm a owner email
     *
     * @param $confirmation_code
     * @return mixed
     */
    public function confirmEmail($confirmation_code)
    {
        $owner = Owner::whereConfirmationCode($confirmation_code)->first();

        if (!$owner) {
            return view('Public.Errors.Generic', [
                'message' => trans("Controllers.confirmation_malformed"),
            ]);
        }

        $owner->is_confirmed = 1;
        $owner->confirmation_code = null;
        $owner->save();

        session()->flash('message', trans("Controllers.confirmation_successful"));

        return redirect()->route('login');
    }
}
