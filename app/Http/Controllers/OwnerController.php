<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use Illuminate\Http\Request;
use validator;

class OwnerController extends Controller
{
    /**
     * Show the edit owner modal
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function showEditOwner()
    {
        $data = [
            'owner' => Auth::owner(),
        ];

        return view('ManageOwner.Modals.EditOwner', $data);
    }

    /**
     * Updates the current owner
     *
     * @param Request $request
     * @return mixed
     */
    public function postEditOwner(Request $request)
    {
        $rules = [
            'email'        => [
                'required',
                'email',
                'unique:owners,email,' . Auth::owner()->id . ',id,account_id,' . Auth::owner()->account_id
            ],
            'password'     => 'passcheck',
            'new_password' => ['min:8', 'confirmed', 'required_with:password'],
            'first_name'   => ['required'],
            'last_name'    => ['required'],
        ];

        $messages = [
            'email.email'         => trans("Controllers.error.email.email"),
            'email.required'      => trans("Controllers.error.email.required"),
            'password.passcheck'  => trans("Controllers.error.password.passcheck"),
            'email.unique'        => trans("Controllers.error.email.unique"),
            'first_name.required' => trans("Controllers.error.first_name.required"),
            'last_name.required'  => trans("Controllers.error.last_name.required"),
        ];

        $validation = Validator::make($request->all(), $rules, $messages);

        if ($validation->fails()) {
            return response()->json([
                'status'   => 'error',
                'messages' => $validation->messages()->toArray(),
            ]);
        }

        $owner = Auth::owner();

        if ($request->get('password')) {
            $owner->password = Hash::make($request->get('new_password'));
        }

        $owner->first_name = $request->get('first_name');
        $owner->last_name = $request->get('last_name');
        $owner->email = $request->get('email');

        $owner->save();

        return response()->json([
            'status'  => 'success',
            'message' => trans("Controllers.successfully_saved_details"),
        ]);
    }
}
