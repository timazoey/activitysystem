<section class="banner">
    <button class="menu">選單</button>
    <button type="button"
            class="user-login"
            data-toggle="modal" 
            data-invoke="modal"
            data-target="#loginModal">
        登入 / 註冊
    </button>
</section>

<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="display:flex;flex-direction: row;justify-content: space-between;">
        <h5 class="modal-title" id="exampleModalLabel">登入帳號</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">帳號: </label>
            <input type="text" class="form-control" id="recipient-name" placeholder="email">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">密碼: 
            </label>
            <input type="text" class="form-control" id="message-text">
          </div>
        </form>
        <button type="button" class="btn btn-secondary">忘記密碼?</button>
        <button type="button" class="btn btn-secondary">我要註冊</button>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
        <button type="button" class="btn btn-primary">確定</button>
      </div>
    </div>
  </div>
</div>

<script>
$('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) 
  var recipient = button.data('whatever')
  
  var modal = $(this)
  modal.find('.modal-title').text('New message to ' + recipient)
  modal.find('.modal-body input').val(recipient)
})
</script>